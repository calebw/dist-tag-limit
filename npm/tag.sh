#!/bin/ash

# Function to generate a random number between min and max
generate_random_number() {
    local min=$1
    local max=$2
    echo $(($min + RANDOM % ($max - $min + 1)))
}

for i in $(seq 0 199); do
    # Generate a random number in the specified range
    rnd=$(generate_random_number 899999999 999999999)

    # Form the tag
    tag="TST-${i}${rnd}"

    # Log the action
    echo "publishing tag $tag"

    # Execute the npm command
    npm dist-tag add @$CI_PROJECT_ROOT_NAMESPACE/$CI_PROJECT_NAME@$VERSION $tag
done
